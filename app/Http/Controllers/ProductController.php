<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    public function getAll(){
        $products = Product::all();
        return view('products.category',['products' => $products]);
    }

    public function getByCategory($id){
        $products = Product::where('category_id', '=', $id)->firstOrFail();
        $category = Category::find($id);
        return view('products.category',['products' => $products,'category' => $category]);
    }

    public function create(Request $req){
        Product::create($req->all());
        return redirect()->back();
    }
}
