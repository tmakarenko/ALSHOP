@extends('layouts.shop')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add product</div>

                    <div class="panel-body">
                        {!! Form::open(['url' => 'product/add','method' => 'post']) !!}
                        <div class='form-group'>
                            {!! Form::label('title', 'Title:') !!}
                            {!! Form::text('title', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class='form-group'>
                            {!! Form::label('description', 'Description:') !!}
                            {!! Form::text('description', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class='form-group'>
                            {!! Form::label('price', 'Price:') !!}
                            {!! Form::number('price', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class='form-group'>
                            {!! Form::label('count', 'Count:') !!}
                            {!! Form::number('count', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class='form-group'>
                            {!! Form::label('img_src', 'Image Link:') !!}
                            {!! Form::text('img_src', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class='form-group'>
                            {!! Form::label('category_id', 'Category:') !!}
                            <select name="category_id" id="category_id">
                                @foreach($categories as $value)
                                    <option value="{!! $value['id'] !!}">{!! $value['name'] !!}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class='form-group'>
                            {!! Form::submit('Add', ['class' => 'btn btn-lg btn-success form-control']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
