@extends('layouts.shop')

@section('content')
    <div class="container">
        <div class="row">

            @foreach($products as $product)
               <div class="col-md-4 col-md-offset-1">
                   <div class="panel panel-default">
                       <div class="panel-heading">
                           <h3 class="panel-title">Title: {{$product['title']}}</h3>
                       </div>
                       <div class="panel-body">
                           Desription: {{$product['description']}}<br>
                           Count:{{$product['count']}}<br>
                           <b>Price: {{$product['price']}}hrn</b>
                       </div>
                   </div>
               </div>
            @endforeach
        </div>
    </div>
@endsection


