<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('/')->group(function(){
    Route::get('','ProductController@getAll');

});

Route::prefix('/category')->group(function(){
    Route::get('/{id}','ProductController@getAll');

});

Route::prefix('/product')->group(function(){
    Route::get('/add',function(){
        return view('products.add',['categories' => \App\Category::all()]);
    });
    Route::post('/add','ProductController@create');

});